package tests;

import junit.framework.*;
import java.text.*;
import java.util.*;

import bank.application.Entity.Cvv;
import bank.check.CheckBuyBank;


public class TestUnits extends TestCase {
	
	
	///
	///
	///		TU
	///
	///
	

	public static void testCheckDateCard() throws Exception {
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
		
		Date date_1 = formatter.parse("01/2017");
	
		assertFalse("Exception Date Card", CheckBuyBank.checkDateCard(date_1));
		
	}
	
	public static void testCheckNumberNumeroCard() throws Exception {
		assertFalse("Exception Number Numero Card", CheckBuyBank.checkNumberNumeroCard("0000000"));
	}
	
	public static void testCheckFormatNumeroCard() throws Exception {
		assertFalse("Exception Format Numero Card", CheckBuyBank.checkFormatNumeroCard("nonononononononononon"));
	}
	
	public static void testCheckFormatCVV() {
		assertTrue("Exception CVV Exist", CheckBuyBank.checkFormatCVV("987"));
	}
	
	public static void testCheckCVVExist() throws Exception {
		
		List<Cvv> cvvs = new ArrayList<Cvv>();
		
		Cvv cvv1 = new Cvv("111");
		Cvv cvv2 = new Cvv("222");
		Cvv cvv3 = new Cvv("333");
		
		cvvs.add(cvv1);
		cvvs.add(cvv2);
		cvvs.add(cvv3);
		
		assertFalse("Exception CVV Exist", CheckBuyBank.checkCVVExist("000", cvvs));
	}
	
	///
	///		TA
	///
	
	public static void testCheckValidationPaiement() throws Exception {
		assertFalse("Exception Validation Paiement", CheckBuyBank.checkValidationPaiement(120, 300, 5));
	}
	
}
