package bank.check;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import bank.application.Entity.Cvv;

public class CheckBuyBank {

	public static boolean checkDateCard(Date date_) throws ParseException {
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy"); 

		Date date = new Date(); // date d'aujourd'hui
		
		if (date.before(date_))
			return true;
		else
			return false;
		
	}
	
	public static boolean checkNumberNumeroCard(String numero_) {
		
		int number = 16;
		
		if (number == numero_.length())
			return true;
		else
			return false;
		
	}
	
	public static boolean checkFormatNumeroCard(String numero_) {
		
		if (numero_.matches("[0-9]"))
			return true;
		else
			return false;
		
	}
	
	public static boolean checkFormatCVV(String cvv_)
	{
		
		if (cvv_.length() == 3 && cvv_.matches("\\d+") && cvv_ != "000")
			return true;
		else
			return false;
	}
	
	public static boolean checkCVVExist(String cvv_, List<Cvv> cvvs_) {
		
		ListIterator<Cvv> it = cvvs_.listIterator();
		
		while (it.hasNext()) {
		
			Cvv cvv = it.next();
			if (cvv.getCvv().equals(cvv_))
				return true;
		}
		
		return false;
		
	}
	
	public static boolean checkValidationPaiement(int paiement_, int accountCount_, int accountCountDiff_) {
		
		if ((paiement_ * 5) <= (accountCount_ - accountCountDiff_))
			return true;
		else
			return false;
	}
	
}
