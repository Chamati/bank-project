package bank.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import bank.application.Entity.Account;
import bank.application.Entity.Cvv;
import bank.check.CheckBuyBank;

public class main {

	///
	///		Prog BANK
	/// 
	///		- Initialisation des éléments de 10 comptes en Random avec un montant de 300 €
	///		- Interface en console
	///
	
	public static void main(String [] args) throws ParseException, InterruptedException
	{
		List<Account> accounts = new ArrayList<Account>();
		List<Cvv> cvvs = new ArrayList<Cvv>(); 
		
		// Initialisation des données BANK comptes
		initBank(accounts, cvvs);
		
		// Faire un achat
		makeAPurchase(accounts, cvvs);
	}
		
	
	public static void makeAPurchase(List<Account> accounts, List<Cvv> cvvs) throws InterruptedException
	{
		
		// Affichage des listes des comptes générés
		showAccountsInConsol(accounts);
		
		
		Scanner scanner = new Scanner(System.in);
		
		int mount;
		String numero_account, date_end, cvv;
		
		System.out.println("\n** Demande de paiement en cours **");
		System.out.println("Le montant de votre achat : ");
		mount = scanner.nextInt();
		scanner.nextLine();
		System.out.println("---");
		
		while (true)
		{
			System.out.println("Numéro de votre compte : ");
			numero_account = scanner.nextLine();
			
			if (CheckBuyBank.checkNumberNumeroCard(numero_account) == false)
				System.out.println("Erreur : votre numéro de carte bleu est composé de 16 chiffres");
			else
				break;
			
			if (CheckBuyBank.checkFormatNumeroCard(numero_account) == false)
				System.out.println("Erreur : le format du numéro de votre carte bleu est invalide ");
			else
				break;
		}
		
		while (true)
		{
			System.out.println("Date d'expiration (au fomat MM/YYYY)°: ");
			date_end = scanner.nextLine();
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
	     
	        Date date;
			try {
				date = formatter.parse(date_end);
				if (CheckBuyBank.checkDateCard(date) == true)
					break;
				else
					System.out.println("Erreur : date non valide");
			} catch (ParseException e) {
					System.out.println("Erreur : format date invalide");
			}
		}
		
		while (true)
		{
			System.out.println("Numéro CVV (au dos de votre carte) : ");
	        cvv = scanner.nextLine();
	        
	        if (CheckBuyBank.checkFormatCVV(cvv) == false)
	        	System.out.println("Erreur : format code cvv invalide");
	        else
	        	break;
	        
	        if (CheckBuyBank.checkCVVExist(cvv, cvvs) == false)
	        	System.out.println("Erreur : code cvv not exist");
	        else
	        	break;
		}
        
        
        ListIterator<Account> it = accounts.listIterator();        
        
        System.out.println("---");
        while (it.hasNext()) {
			
        	Account account = it.next();
        	if (account.getCard_numero().equals(numero_account) && account.getCard_cvv().equals(cvv) == true)
        	{	
        		
        		
                System.out.print("\nDemande envoyé à la banque");
                TimeUnit.SECONDS.sleep(1);
                System.out.print(".");
                TimeUnit.SECONDS.sleep(1);
                System.out.println(".");
                TimeUnit.SECONDS.sleep(1);
                
                System.out.println("---");
                
                if (CheckBuyBank.checkValidationPaiement(mount, account.getCount_of_account(), account.getCount_of_account_diff()) == true)
                {
                	account.setCount_of_account_diff( account.getCount_of_account_diff() + mount );
                	
                	System.out.println("\n");
                	System.out.println("** Identification validé !");

                	TimeUnit.SECONDS.sleep(2);
                	
                	makeAPurchase(accounts, cvvs);
                }
                else
                {
                	
                	System.out.println("\n");
                	System.out.println("** Paiement non accepté !");
                	
                	TimeUnit.SECONDS.sleep(1);

                	System.out.println("Votre compte n'est pas assez appovisioner.");
                	
                	TimeUnit.SECONDS.sleep(2);
                	
                	makeAPurchase(accounts, cvvs);
                }
        	}
        }
        
    	System.out.println("** Identification non valide");
    	System.out.println("Veuillez réessayer : ");
    	makeAPurchase(accounts, cvvs);
    	
	}
	
	//
	//	Afficher la liste des comptes initialisés
	
	public static void showAccountsInConsol(List<Account> accounts) {
		
		ListIterator<Account> it = accounts.listIterator();
		
		System.out.println("LISTE DES COMPTES :");
		
		while (it.hasNext()) {
			Account account = it.next();
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
			
			System.out.println("Identité de la personne : " + account.getFirstname() + " " + account.getLastname());
			System.out.println("Numero de carte bleu : " + account.getCard_numero());
			System.out.println("Numéro CVV : " + account.getCard_cvv());
			System.out.println("Date de fin de carte : " + simpleDateFormat.format(account.getCard_date_end()));
			System.out.println("--");
			System.out.println("Montant du compte en banque : " + account.getCount_of_account() + " €");
			System.out.println("Montant en cours de transition : " + account.getCount_of_account_diff() + " €");
			
			System.out.println("\n-----------------------------\n");
		}
	}

	//
	//	Initialiser la liste des comptes de la BANK
	
	public static List<Account> initBank(List<Account> accounts, List<Cvv> cvvs) throws ParseException
	{
		
		for (int i = 0; i <= (10 - 1); i++)
		{
			Account account = new Account();
			Cvv cvv = new Cvv();
			
			account.setFirstname("--------");
			
			account.setLastname("--------");
			
			String numero_account = "";
			for (int index_numero = 0; index_numero <= (16 - 1); index_numero++) {
				
				Random rand = new Random();
				int randomNum = rand.nextInt((9 - 0) + 1) + 0;
				char number = Integer.toString(randomNum).charAt(0);
				
				numero_account = numero_account + number;
			}
			
			account.setCard_numero(numero_account);
			
			String cvv_account = "";
			for (int index_cvv = 0; index_cvv <= (3 - 1); index_cvv++) {
				
				Random rand = new Random();
				int randomNum = rand.nextInt((9 - 0) + 1) + 0;
				char number = Integer.toString(randomNum).charAt(0);
				
				cvv_account = cvv_account + number;
			}
			
			account.setCard_cvv(cvv_account);
			
			
			cvv.setCvv(cvv_account);
			
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			Random rand = new Random();
			Date date = simpleDateFormat.parse( Integer.toString(rand.nextInt((30 - 1) + 1) + 1) + "/" + Integer.toString(rand.nextInt((12 - 1) + 1) + 1) + "/20" + Integer.toString(rand.nextInt((22 - 18) + 1) + 18));
			
			account.setCard_date_end(date);
		
			
			accounts.add(account);
			cvvs.add(cvv);
			
		}
		
		return accounts;
	}
}
