package bank.application.Entity;

import java.text.*;
import java.util.*;

public class Account {

	private String firstname;
	private String lastname;
	private Date card_date_end;
	private String card_numero;
	private String card_cvv;
	private int count_of_account;
	private int count_of_account_diff;
	
	public Account() {
		
		firstname = "";
		lastname = "";
		
		card_date_end = new Date();
		card_numero = "";
		card_cvv = "000";
		count_of_account = 300;
		count_of_account_diff = 0;
	}
	
	public Date getCard_date_end() {
		return card_date_end;
	}
	public void setCard_date_end(Date card_date_end) {
		this.card_date_end = card_date_end;
	}
	public String getCard_numero() {
		return card_numero;
	}
	public void setCard_numero(String card_numero) {
		this.card_numero = card_numero;
	}
	public String getCard_cvv() {
		return card_cvv;
	}
	public void setCard_cvv(String card_cvv) {
		this.card_cvv = card_cvv;
	}
	public int getCount_of_account() {
		return count_of_account;
	}
	public void setCount_of_account(int count_of_account) {
		this.count_of_account = count_of_account;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getCount_of_account_diff() {
		return count_of_account_diff;
	}

	public void setCount_of_account_diff(int count_of_account_diff) {
		this.count_of_account_diff = count_of_account_diff;
	}
	
}
