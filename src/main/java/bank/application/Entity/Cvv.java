package bank.application.Entity;

public class Cvv {

	private String cvv;
	
	public Cvv() {
		
	}
	
	public Cvv(String cvv_) {
		cvv = cvv_;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
}
