# Version: 0.1
FROM ubuntu:14.04

# data repository
RUN mkdir -p /data/db
VOLUME ['/data/db']

# package
RUN apt-get update && \
    apt-get install -y --force-yes openjdk-7-jre maven
ADD target/bank-project-0.0.1-SNAPSHOT.jar /data/db/bp.jar 

